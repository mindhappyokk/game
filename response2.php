<?php 

	    session_start();


		$count = $_SESSION["count"];
		$count2 = $_SESSION["count2"];
		$count3 = $_SESSION["count3"];
		$count4 = $_SESSION["count4"];
		$count5 = $_SESSION["count5"];

		if($count == $count5 && $count4 > $count2 && $count4 > $count3){
			header('Location:Openness_song.html');
		}

		if($count == $count5 && $count2 > $count4 && $count2 > $count4){
			header('Location:Extraversion_song.html');
		}

		if($count2 == $count5){
			header('Location:Openness_song.html');
		}


		if($count > $count2 && $count > $count3 && $count > $count4 && $count > $count5){
			header('Location:Extraversion_song.html');
		}else if($count2 > $count && $count2 > $count3 && $count2 > $count4 && $count2 > $count5){
			header('Location:Agreeable_song.html');
		}else if($count3 > $count && $count3 > $count2 && $count3 > $count4 && $count3 > $count5){
			header('Location:Conscientiousness_song.html');
		}else if($count4 > $count && $count4 > $count3 && $count4 > $count2 && $count4 > $count5){
			header('Location:Neuroticsiam_song.html');
		}else if($count5 > $count && $count5 > $count3 && $count5 > $count2 && $count5 > $count4){
			header('Location:Openness_song.html');
		}

		echo "count" .$count ."<br>";
		echo "count2" .$count2 ."<br>";
		echo "count3" .$count3 ."<br>";
		echo "count4" .$count4 ."<br>";
		echo "count5" .$count5 ."<br>";

		
		//header('Location:feeling.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Music Game</title>
	<style>
		 a:link, a:visited {
		  color: white;
		  padding: 15px 25px;
		  position: center;
		  text-decoration: none;
		  display: inline-block;
		}

	
}
	#hidden_div {
	    display: none;
	}
	</style>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>


	<div class="container-contact100">
		<div class="wrap-contact100">


			<form class="contact100-form validate-form" method="post">
				<span class="contact100-form-title">
					Response
				</span>

				<div>
					<img src="images/index.png" width="100%">
				</div>
				<br>

				<li>Please wait for a minute.</li>


				<div class="container-contact100-form-btn">
					<div class="wrap-contact100-form-btn">
						<div class="contact100-form-bgbtn"></div>
						<button class="contact100-form-btn" name="submit">
							<div align="center">
							<span>
								Play again
								<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
							</span>
							</div>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>



	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>

<!--=============
	==================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>
</html>
