

<?php


	
	session_start();

	$password = $_SESSION["password"];

	//echo "password ".$password ."<br>";

	$ipaddress = $_SESSION["IDad"];




	$conn = mysqli_connect("localhost","root",$password,"Game");
	//$db = mysql_select_db("Gamesurvey", $connection); // Selecting Database from Server
	
	if(!$conn){
		die('could not connect: '. mysqli_connect_error() );
	}


	//echo "why not post \n";	
	//$ID = mysqli_insert_id($conn);


	//echo "New record has id: " . mysqli_insert_id($ID);

	if (isset($_POST['back']))
	
	{

		header('Location:info.php');


	}

  	
	if (isset($_POST['submit']))
	
	{

		$Musical_training =$_POST['Musical_training'];
		$trained = $_POST['been_trained'];
		$instrument = $_POST['play_instrument'];
		$musician = $_POST['Rmusician'];
		$listeninghabit = $_POST['listeninghabit'];	


		//echo "my ID is".$_SESSION["ID"];



		$stml = $conn->prepare("INSERT INTO BackgroudMusic(ID,MusicalTraining,TrainedYear,Abilityplayinstruments,Musician,Listeninghabit) VALUES(?,?,?,?,?,?)");


		$stml->bind_param("ssssss",$ipaddress,$Musical_training,$trained,$instrument,$musician,$listeninghabit);
		//$stml->execute();


		if(!$stml->execute()){
			echo "not too";
		}

		//$_SESSION["ID"]=$ID;

		echo "Session variables are set.";

		//echo "value = ".$_SESSION["ID"];

		
		//echo "New record has id: " .$ID;


		$stml->close();
			
	  	$conn->close();

	  	header('Location:personality.php');

	}

	//mysql_close($connection);
	
?>





<!DOCTYPE html>
<html lang="en">
<head>
	<title>Music Game</title>
	<style>
		 
	#hidden_div {
	    display: none;
	}

	#hidden_div0 {
	    display: none;
	}
	</style>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>


	<div class="container-contact100">
		<div class="wrap-contact100">
			<form class="contact100-form validate-form" method="post">
				<span class="contact100-form-title">
					Background Music
				</span>

				<div class="wrap-input100 validate-input">
					<span class="label-input100">Have any musical training?</span>

					<div>
						<select class="selection-2" name="Musical_training" id="Musical_training" value="Musical_training" onchange="showDiv0('hidden_div0', this)" required>
							<option value="">Choose</option> 
							<option name="Yes" value="Yes">Yes</option>
							<option name="No" value="No">No</option>
							
						</select>
					</div>
				</div>

				<div class="wrap-input100 validate-input" id="hidden_div0">
					<span class="label-input100">How long have you been trained?</span>

					<div>
						<select class="selection-2" name="been_trained" id="been_trained" value="been_trained">
							<option value="">Choose</option> 
							<option name="less than 1 Year (น้อยกว่า 1 ปี)" value="less than 1 Year">less than 1 Year</option>
							<option name="1-3 Year" value="1-3 Year">1-3 Year</option>
							<option name="3-5 Year" value="3-5 Year">3-5 Year</option>
							<option name="5-7 Year" value="5-7 Year">5-7 Year</option>
							<option name="more than 7 Year (มากกว่า 7 ปี)" value="more than 7 Year">more than 7 Year</option>
							
						</select>
					</div>
				</div>
				

				<div class="wrap-input100 validate-input">
					<span class="label-input100" >Ability to play an instrument?</span>


						<select class="selection-2" name="play_instrument" id="play_instrument" value="play_instrument" onchange="showDiv('hidden_div', this)" required>
							<option value="">Choose</option> 
							<option name="Can" value="Can">Can</option>
							<option name="Cannot " value="Cannot">Cannot</option>
						</select>
						
				</div>
			

				<div class="wrap-input100 validate-input" id="hidden_div">
							<span class="label-input100">Are you a musician?</span>
								<select class="selection-2" name="Rmusician" id="Rmusician" value="Rmusician">
									<option value="">Choose</option> 
									<option name="Yes" value="Yes">Yes</option>
									<option name="No" value="No">No</option>
								</select>
				</div>

				
				

				
				<div class="wrap-input100 input100-select">
					<span class="label-input100">Music listening habit</span>
					<div>
						<select class="selection-2" name="listeninghabit" id="listeninghabit" value="listeninghabit" required>
							<option value="">Choose</option> 
							<option name="Rare (ไม่ค่อยฟัง)" value="Rare">Rare</option>
							<option name="Once in a while (นานๆครั้ง)" value="Once in a while">Once in a while</option>
							<option name="Few times a week (สัปดาห์ละหลายครั้ง)" value="Few times a week">Few times a week</option>
							<option name="Daily (ทุกวัน)" value="Daily">Daily</option>
							<option name="All the time (ตลอดเวลา)" value="All the time">All the time</option>							
						</select>
					</div>
					<span class="focus-input100"></span>
				</div>

				

				<div class="container-contact100-form-btn">
					<div class="wrap-contact100-form-btn">
						<div class="contact100-form-bgbtn"></div>
						<button class="contact100-form-btn" type="submit" name="submit">
							<div style="text-align: center;">
								<span align="center">
									Next
									<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
								</span>
							</div>
						</button>
					</div>
				</div>


				


			</form>
		</div>
	</div>


	<script>
		
		function showDiv0(divId, element)
			{
				document.getElementById(divId).style.display = element.value == "Yes" ? 'block' : 'none';
			}


		function showDiv(divId, element)
			
			{
				document.getElementById(divId).style.display = element.value == "Can" ? 'block' : 'none';
			}

	
	</script>



	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>

<!--=============
	==================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>
</html>
