<!DOCTYPE html>
<html lang="en">
<head>
	<title>Music Game</title>
	<style>
		 a:link, a:visited {
		  color: white;
		  padding: 15px 25px;
		  position: center;
		  text-decoration: none;
		  display: inline-block;
		}

	
}
	#hidden_div {
	    display: none;
	}
	</style>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>


	<div class="container-contact100">
		<div class="wrap-contact100">


			<form class="contact100-form validate-form" action="index.php">
				<span class="contact100-form-title">
					You are Neuroticism
				</span>

				<div>
					<img src="images/neuro.jpg" width="100%">
				</div>
				<br>

				<li>เป็นแนวโน้มในการเกิดประสบการณ์อารมณ์ทางลบของบุคคล เป็นความบกพร่องในการปรับตัวทางอารมณ์ของบุคคล ซึ่งเป็นแนวโน้มที่จะเกิดภาวะความเครียดทางจิตใจ เช่น ความรู้สึกวิตกกังวล ไม่มั่นคง ประหม่า กลัว ซึมเศร้า โกรธ นอกจากนี้ยังรวมไปถึงภาวะอารมณ์ที่สับสนที่แทรกซ่อนต่อการปรับตัวด้วย บุคคลที่มีบุคลิกภาพด้านนี้สูง มีแนวโน้มที่จะมีความคิดที่ขาดเหตุผล มีความสามารถในการควบคุมอารมณ์ของตนเองน้อย และเผชิญต่อความเครียดได้ไม่ดีเท่าคนอื่น และอาจเสี่ยงต่อการมีปัญหาทางจิตบางชนิดได้ </li>

				<br>
				<p>Credit: https://medium.com/@FLRTH/บุคลิก-5-ประการ-ให้คุณเลือกงานที่ใช่-5cbf1656cbd5</p>
				<br>


				<p>อยากรู้เกี่ยวกับเพลงที่เหมาะกับบุคลิกภาพนี้คลิก Play more</p>


				<div class="container-contact100-form-btn">
					<div class="wrap-contact100-form-btn">
						<div class="contact100-form-bgbtn"></div>
						<button class="contact100-form-btn" name="submit">
							<div align="center">
							<span>
								Play more
								<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
							</span>
							</div>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>



	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="main.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>

<!--=============
	==================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
