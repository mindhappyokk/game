
<?php

	$password = "root";

	session_start();
	
	$_SESSION["password"]= $password;

	$conn = mysqli_connect("localhost","root",$password,"Game");
	//$db = mysql_select_db("Gamesurvey", $connection); // Selecting Database from Server
	
	if(!$conn){
		die('could not connect: '. mysqli_connect_error() );
	}

	$ipaddress = getenv('REMOTE_ADDR');

	//$MAC = $_SERVER['HTTP_CLIENT_IP']);
  
	// Storing 'getmac' value in $MAC 
	//$MAC = strtok($MAC, ' '); 
	//echo "MAC ".$MAC;


  	
	if (isset($_POST['submit']))
	
	{


		$Gender =$_POST['gender'];
		$Age = $_POST['age'];
		$Degree = $_POST['degree'];
		$Major = $_POST['faculty'];
		$Cultural = $_POST['country'];	


	$stml = $conn->prepare("INSERT INTO Information(Gender,Age,Degree,Major,Cultural,ID) VALUES(?,?,?,?,?,?)");
	$stml->bind_param("ssssss",$Gender,$Age,$Degree,$Major,$Cultural,$ipaddress);
	//$stml->execute();

	//$ip = TIME(expr);

	//echo ."IP".$ip;


	if(!$stml->execute()){
		echo "not too";
	}

	$ID = mysqli_insert_id($conn);

	echo "start";

	echo $ID;

	session_start();
	
	$_SESSION["ID"]=$ID;
	echo "Session variables are set.";

	echo "value".$_SESSION["ID"];

	

	$stml->close();
	$conn->close();
		

  	header('Location:backgroudMusic-TH.php');
	

	}

	//mysql_close($connection);
	
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style>
		
	#hidden_div {
	    display: none;
	}


	</style>
	<title>Music Game</title>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>


	<div class="container-contact100">
		<div class="wrap-contact100">
			<form class="contact100-form validate-form" method="post">
				<span class="contact100-form-title">
					ข้อมูลเกี่ยวกับคุณ 
				</span>


				<div class="wrap-input100 validate-input">
					<span class="label-input100">เพศ</span>

					<div>
						<select class="selection-2" id="gender" name="gender" required>

							<option value="">เลือก</option>
							<option name="Male (ผู้ชาย)" value="Male">ผู้ชาย</option>
							<option name="Female (ผู้หญิง)" value="Female">ผู้หญิง</option>
							<option name="Nonbinary (ไม่ใช่ทั้งผู้หญิงและผู้ชาย)" value="Nonbinary">ไม่ใช่ทั้งผู้หญิงและผู้ชาย</option>
							
						</select>
					</div>
				</div>

				<div class="wrap-input100 input100-select">
					<span class="label-input100">อายุ</span>
					<div>
						<select class="selection-2" name="age" id="age" required>


							<option value="">เลือก</option>
							<option name="< 18" value="< 18"> < 18 </option>
							<option name="18 - 24" value="18 - 24">18 - 24 </option>
							<option name="25 - 34" value="25 - 34">25 - 34 </option>
							<option name="35 - 44" value="35 - 44">35 - 44</option>
							<option name="45 - 54" value="45 - 54">45 - 54</option>
							<option name=" > 54" value=" > 54"> > 54</option>
						</select>
					</div>
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 input100-select">

					<span class="label-input100">ระดับการศึกษา</span>
					<div>
						<select class="selection-2" name="degree" id="degree" required>

							<option value="">เลือก</option>
							<option name="High-school Certificate (จบมัธยมปลาย)" value="High-school Certificate">จบมัธยมปลาย</option>
							<option name="Undergraduate student (กำลังศึกษาระดับปริญญาตรี)" value="Undergraduate student">กำลังศึกษาระดับปริญญาตรี</option>
							<option name="Bachelor degree (จบการศึกษาระดับปริญญาตรี)" value="Bachelor degree">จบการศึกษาระดับปริญญาตรี</option>
							<option name="Graduate student (กำลังศึกษาระดับปริญญาโท)" value="Graduate student">กำลังศึกษาระดับปริญญาโท</option>
							<option name="Master degree (จบการศึกษาระดับปริญญาโท)" value="Master degree">จบการศึกษาระดับปริญญาโท</option>
							<option name="Ph.D. student (กำลังศึกษาระดับปริญญาเอก)" value="Ph.D. student">กำลังศึกษาระดับปริญญาเอก</option>
							<option name="Ph.D. degree (จบการศึกษาระดับปริญญาเอก)" value="Ph.D. degree">จบการศึกษาระดับปริญญาเอก</option>
							<option name="Other (อื่นๆ)" value="Other">อื่นๆ</option>
						</select>
					</div>
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 input100-select">
					<span class="label-input100">กลุ่มวิชาเอก</span>
					
					<div>
						<select class="selection-2" name="faculty" id="faculty" required>
							<option value="">เลือก</option>
							<option name="Law Group (กลุ่มกฎหมาย)" value="Law Group">กลุ่มกฎหมาย</option>
							<option name="Liberal Arts or Social Group (กลุ่มศิลปศาสตร์หรือกลุ่มสังคมศาสตร์)" value="Liberal Arts or Social Group">กลุ่มศิลปศาสตร์หรือกลุ่มสังคมศาสตร์</option>
							<option name="Science Management Group (กลุ่มพาณิชยศาสตร์และกลุ่มการจัดการ)" value="Science Management Group">กลุ่มพาณิชยศาสตร์และกลุ่มการจัดการ</option>
							<option name="Engineer and Science Group (กลุ่มวิศวกรรมศาสตร์และกลุ่มวิทยาศาสตร์)" value="Engineer and Science Group">กลุ่มวิศวกรรมศาสตร์และกลุ่มวิทยาศาสตร์</option>
							<option name="Medicine Group (กลุ่มแพทย์ กลุ่มพยาบาล กลุ่มเทคนิคการแพทย์)" value="Medicine Group">กลุ่มแพทย์ กลุ่มพยาบาล กลุ่มเทคนิคการแพทย์</option>
							<option name="Other (อื่นๆ)" value="Other">อื่นๆ</option>
						</select>
					</div>
					<span class="focus-input100"></span>
				</div>

				
				<div class="wrap-input100 input100-select">
					<span class="label-input100">เชื้อชาติ</span>
					<div>
						<select class="selection-2" name="country" id="country" required>
							<option value="">เลือก</option>
							<option name="Thai (ไทย)" value="Thai">ไทย</option>
							<option name="Taiwan (ไต้หวัน)" value="Taiwan">ไต้หวัน</option>
							<option name="Chinese (จีน)" value="Chinese">จีน</option>
							<option name="British (อังกฤษ)" value="British">อังกฤษ</option>
							<option name="European (ยุโรป)" value="European">ยุโรป</option>
							<option name="American (อเมริกา)" value="American">อเมริกา</option>
							<option name="Other (อื่นๆ)" value="Other">อื่นๆ</option>
							
						</select>

						
					</div>
					<span class="focus-input100"></span>
				</div>

				<div class="container-contact100-form-btn">
					<div class="wrap-contact100-form-btn">
						<div class="contact100-form-bgbtn"></div>
						<button class="contact100-form-btn" type="submit" name="submit">
							<div style="text-align: center;">
								<span align="center">
									ถัดไป
									<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
								</span>
							</div>
						</button>
					</div>
				</div>
			</form>
		</div>

	</div>

	




	

	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="https://connect.facebook.net/en_US/fbinstant.6.3.js"></script>


	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>
</html>





